﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public UIControl ui;

    public Transform player, centerOfArena;
    public Transform ramuhRadiusCheck, leviathanRadiusCheck;

    public SpriteRenderer leviathan, garuda, ramuh, ifrit, leviathan_beam, garuda_beam, ramuh_beam, ifrit_beam;

    public float idlingTime, castingTime, stockingTime, releasingTime, effectTime;

    private AudioSource music;
    public AudioSource voiceLine;
    public AudioClip[] voiceLines;
    int voiceLineCounter = 0;

    float timer;
    float castEffectTimer;

    bool idling, casting, stocking, releasing, castEffect;

    bool stocked;

    bool leviathanStocked, garudaStocked, ramuhStocked, ifritStocked;
    bool leviathanTethered, garudaTethered, ramuhTethered, ifritTethered;

    int vulnCounter;
    float ramuhSize, leviathanMinX, leviathanMaxX;

    bool playVoiceLines;

    public void ResetVulns()
    {
        vulnCounter = 0;
        ui.VulnUp(0);
    }

    public void ToggleMusic(bool toggle)
    {
        music.volume = toggle ? 0.5f : 0;
    }

    public void ToggleVoice(bool toggle)
    {
        voiceLine.volume = toggle ? 1 : 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        idling = true;
        music = GetComponent<AudioSource>();
        music.time = 2.7f;
        music.Play();
        ui.EndAbility();

        ramuhSize = ramuhRadiusCheck.position.x - centerOfArena.position.x;
        leviathanMaxX = leviathanRadiusCheck.position.x;
        leviathanMinX = centerOfArena.position.x - (leviathanRadiusCheck.position.x - centerOfArena.position.x);

        playVoiceLines = true;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(idling)
        {
            if(timer > idlingTime)
            {
                timer -= idlingTime;
                idling = false;
                if(stocked)
                {
                    if(Random.Range(0, 3) < 2)
                    {
                        casting = true;
                        ui.StartAbility("Casting");
                        ui.SetMax(castingTime);
                        tether();
                    }
                    else
                    {
                        releasing = true;
                        playNextLine();
                        ui.StartAbility("Releasing");
                        ui.SetMax(releasingTime);
                    }
                }
                else
                {
                    if (Random.Range(0, 3) < 2)
                    {
                        casting = true;
                        ui.StartAbility("Casting");
                        ui.SetMax(castingTime);
                        tether();
                    }
                    else
                    {
                        stocking = true;
                        ui.StartAbility("Stocking");
                        ui.SetMax(stockingTime);
                        tether();
                    }
                }
            }
        }
        else if(casting)
        {
            ui.SetValue(timer);
            if (timer > castingTime)
            {
                ui.EndAbility();
                timer -= castingTime;
                cast();
                casting = false;
                idling = true;
            }
        }
        else if (stocking)
        {
            ui.SetValue(timer);
            if (timer > stockingTime)
            {
                ui.EndAbility();
                timer -= stockingTime;
                stock();
                stocking = false;
                stocked = true;
                idling = true;
            }
        }
        else if (releasing)
        {
            ui.SetValue(timer);
            if (timer > releasingTime)
            {
                ui.EndAbility();
                timer -= releasingTime;
                cast();
                releasing = false;
                stocked = false;
                idling = true;
            }
        }

        if(castEffect)
        {
            castEffectTimer += Time.deltaTime;
            if(castEffectTimer > effectTime)
            {
                castEffectTimer = 0;
                castEffect = false;
                garuda.enabled = false;
                ifrit.enabled = false;
                ramuh.enabled = false;
                leviathan.enabled = false;
            }
        }
    }

    void tether()
    {
        switch(Random.Range(0, 5))
        {
            case 0:
                leviathanTethered = true;
                ramuhTethered = true;
                break;
            case 1:
                leviathanTethered = true;
                garudaTethered = true;
                break;
            case 2:
                leviathanTethered = true;
                ifritTethered = true;
                break;
            case 3:
                ramuhTethered = true;
                ifritTethered = true;
                break;
            case 4:
                ramuhTethered = true;
                garudaTethered = true;
                break;
        }
        playNextLine();
        updateTethers();
    }

    void cast()
    {
        castEffect = true;
        if(casting)
        {
            bool hit = false;
            if(ramuhTethered)
            {
                ramuhTethered = false;
                hit |= ramuh_activate();
            }
            if (ifritTethered)
            {
                ifritTethered = false;
                hit |= ifrit_activate();
            }
            if (garudaTethered)
            {
                garudaTethered = false;
                hit |= garuda_activate();
            }
            if (leviathanTethered)
            {
                leviathanTethered = false;
                hit |= leviathan_activate();
            }
            if (hit) get_hit();
        }
        else if (releasing)
        {
            bool hit = false;
            if (ramuhStocked)
            {
                ramuhStocked = false;
                hit |= ramuh_activate();
            }
            if (ifritStocked)
            {
                ifritStocked = false;
                hit |= ifrit_activate();
            }
            if (garudaStocked)
            {
                garudaStocked = false;
                hit |= garuda_activate();
            }
            if (leviathanStocked)
            {
                leviathanStocked = false;
                hit |= leviathan_activate();
            }
            if (hit) get_hit();
        }
        updateTethers();
    }

    void updateTethers()
    {
        leviathan_beam.enabled = leviathanTethered;
        garuda_beam.enabled = garudaTethered;
        ramuh_beam.enabled = ramuhTethered;
        ifrit_beam.enabled = ifritTethered;
    }

    void stock()
    {
        ramuhStocked = ramuhTethered;
        ifritStocked = ifritTethered;
        garudaStocked = garudaTethered;
        leviathanStocked = leviathanTethered;
        ramuhTethered = false;
        ifritTethered = false;
        garudaTethered = false;
        leviathanTethered = false;
        updateTethers();
    }

    bool ramuh_activate()
    {
        ramuh.enabled = true;
        var distance = Mathf.Sqrt(Mathf.Pow(player.position.x - centerOfArena.position.x, 2) + Mathf.Pow(player.position.y - centerOfArena.position.y, 2));
        var hit = (distance < ramuhSize);
        //Debug.Log(string.Format("Ramuh {0}> Player pos {1}:{2} | Arena center {3}:{4} | Radius {5} | Distance {6}", hit, player.position.x, player.position.y, centerOfArena.position.x, centerOfArena.position.y, ramuhSize, distance));
        return hit;
    }

    bool ifrit_activate()
    {
        ifrit.enabled = true;
        var angle = Vector2.Angle(player.position - centerOfArena.position, Vector2.down);
        var hit = angle < 67.5f || angle > 112.5f;
       // Debug.Log(string.Format("Ifrit {0}> Player pos {1}:{2} | Arena center {3}:{4} | angle {5}", hit, player.position.x, player.position.y, centerOfArena.position.x, centerOfArena.position.y, angle));
        return hit;
    }

    bool garuda_activate()
    {
        garuda.enabled = true;
        var angle = Vector2.Angle(player.position - centerOfArena.position, Vector2.down);
        var hit = angle < 22.5f || angle > 157.5f || (angle > 67.5f && angle < 112.5f);
        //Debug.Log(string.Format("Garuda {0}> Player pos {1}:{2} | Arena center {3}:{4} | angle {5}", hit, player.position.x, player.position.y, centerOfArena.position.x, centerOfArena.position.y, angle));
        return hit;
    }

    bool leviathan_activate()
    {
        leviathan.enabled = true;
        var hit = player.position.x < leviathanMinX || player.position.x > leviathanMaxX;
        //Debug.Log(string.Format("Leviathan {0}> Player pos {1}:{2} | Levi borders {3}:{4}", hit, player.position.x, player.position.y, leviathanMinX, leviathanMaxX));
        return hit;
    }

    void get_hit()
    {
        ui.VulnUp(++vulnCounter);
    }

    void playNextLine()
    {
        if (playVoiceLines)
        {
            voiceLine.Stop();
            voiceLine.clip = voiceLines[voiceLineCounter++];
            if (voiceLineCounter >= voiceLines.Length) voiceLineCounter = 0;
            voiceLine.Play();
        }
    }
}
