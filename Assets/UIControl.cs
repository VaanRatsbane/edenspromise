﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    public Slider slider;
    public TMPro.TextMeshProUGUI ability, vulns;

    public void StartAbility(string abilityText)
    {
        slider.gameObject.SetActive(true);
        ability.text = abilityText;
    }

    public void EndAbility()
    {
        slider.gameObject.SetActive(false);
        ability.text = string.Empty;
    }
    public void SetMax(float max)
    {
        slider.maxValue = max;
        slider.value = 0;
    }

    public void SetValue(float value)
    {
        slider.value = value;
    }

    public void VulnUp(int value)
    {
        vulns.text = $"{value}";
    }
}
