﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float xMovement, yMovement;

    public float speed;
    public CircleCollider2D arena;

    float arenaRadius = 3.7f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        xMovement = Input.GetAxis("Horizontal");
        yMovement = Input.GetAxis("Vertical");
        var movement = new Vector2(xMovement, yMovement) * speed * Time.deltaTime;
        transform.position = LimitPlayerMovementWithinArena(transform.position, arena.transform.position, movement);
    }

    Vector2 LimitPlayerMovementWithinArena(Vector2 player, Vector2 arenaOrigin, Vector2 movement)
    {
        var newPos = player + movement;
        if ((Mathf.Sqrt(Mathf.Pow(newPos.x - arenaOrigin.x, 2) + Mathf.Pow(newPos.y - arenaOrigin.y, 2)) > arenaRadius))
            return player;
        return newPos;
    }
}
